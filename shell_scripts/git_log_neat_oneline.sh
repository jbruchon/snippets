#!/bin/sh

git log --pretty=format:'%C(auto)%h%C(green) %<|(19)%ai%C(auto)%d %s'
