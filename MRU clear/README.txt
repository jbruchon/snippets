These registry and batch files clear Most Recently Used
lists in Windows. They may have unintended side effects,
so be careful when using them...especially the ones that
affect Explorer and "shell bags."
